package structs

import "time"

type Player struct {
	TrackedUntil        string `json:"tracked_until"`
	SoloCompetitiveRank string `json:"solo_competitive_rank"`
	CompetitiveRank     string `json:"competitive_rank"`
	RankTier            int    `json:"rank_tier"`
	LeaderboardRank     int    `json:"leaderboard_rank"`
	MmrEstimate         struct {
		Estimate int `json:"estimate"`
	} `json:"mmr_estimate"`
	Profile Profile `json:"profile"`
}

type Profile struct {
	AccountID       int       `json:"account_id"`
	Steamid         string    `json:"steamid"`
	Avatar          string    `json:"avatar"`
	Avatarmedium    string    `json:"avatarmedium"`
	Avatarfull      string    `json:"avatarfull"`
	Profileurl      string    `json:"profileurl"`
	Personaname     string    `json:"personaname"`
	LastLogin       time.Time `json:"last_login"`
	FullHistoryTime time.Time `json:"full_history_time"`
	Cheese          int       `json:"cheese"`
	FhUnavailable   bool      `json:"fh_unavailable"`
	Loccountrycode  string    `json:"loccountrycode"`
	Name            string    `json:"name"`
	CountryCode     string    `json:"country_code"`
	FantasyRole     int       `json:"fantasy_role"`
	TeamID          int       `json:"team_id"`
	TeamName        string    `json:"team_name"`
	TeamTag         string    `json:"team_tag"`
	IsLocked        bool      `json:"is_locked"`
	IsPro           bool      `json:"is_pro"`
	LockedUntil     int       `json:"locked_until"`
}

type PlayerWL struct {
	Win  int `json:"win"`
	Lose int `json:"lose"`
}

type PlayerRecentMatches []struct {
	MatchID      int  `json:"match_id"`
	PlayerSlot   int  `json:"player_slot"`
	RadiantWin   bool `json:"radiant_win"`
	Duration     int  `json:"duration"`
	GameMode     int  `json:"game_mode"`
	LobbyType    int  `json:"lobby_type"`
	HeroID       int  `json:"hero_id"`
	StartTime    int  `json:"start_time"`
	Version      int  `json:"version"`
	Kills        int  `json:"kills"`
	Deaths       int  `json:"deaths"`
	Assists      int  `json:"assists"`
	Skill        int  `json:"skill"`
	XpPerMin     int  `json:"xp_per_min"`
	GoldPerMin   int  `json:"gold_per_min"`
	HeroDamage   int  `json:"hero_damage"`
	HeroHealing  int  `json:"hero_healing"`
	LastHits     int  `json:"last_hits"`
	Lane         int  `json:"lane"`
	LaneRole     int  `json:"lane_role"`
	IsRoaming    bool `json:"is_roaming"`
	Cluster      int  `json:"cluster"`
	LeaverStatus int  `json:"leaver_status"`
	PartySize    int  `json:"party_size"`
}
