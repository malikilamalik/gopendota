package gopendota

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/malikilamalik/gopendota/structs"
)

//Error
var ErrorMatchNotFound string = "matches: match not found"

// GetMatchByMatchID
func (c Client) GetMatchByMatchID(matchID string) (match structs.Match, err error) {
	url := EndpointMatch(matchID)

	if c.ApiKey != nil {
		resp, err := c.Client.R().
			SetQueryParams(map[string]string{
				"api_key": *c.ApiKey,
			}).
			SetHeader("Accept", "application/json").
			Get(url)

		if err != nil {
			return structs.Match{}, err
		}

		if resp.StatusCode() == http.StatusNotFound {
			return structs.Match{}, errors.New(ErrorMatchNotFound)
		}

		json.Unmarshal([]byte(resp.Body()), &match)
		return match, nil
	}

	resp, err := c.Client.R().
		SetHeader("Accept", "application/json").
		Get(url)

	if err != nil {
		return structs.Match{}, err
	}

	if resp.StatusCode() == http.StatusNotFound {
		return structs.Match{}, errors.New(ErrorMatchNotFound)
	}

	json.Unmarshal([]byte(resp.Body()), &match)
	return match, nil
}
