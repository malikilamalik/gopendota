package gopendota

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/malikilamalik/gopendota/structs"
)

// GetProPlayers
func (c Client) GetProPlayers() (profile []structs.Profile, err error) {
	url := EndpointProPlayers

	if c.ApiKey != nil {
		resp, err := c.Client.R().
			SetQueryParams(map[string]string{
				"api_key": *c.ApiKey,
			}).
			SetHeader("Accept", "application/json").
			Get(url)

		if err != nil {
			return []structs.Profile{}, err
		}

		if resp.StatusCode() == http.StatusNotFound {
			return []structs.Profile{}, errors.New(ErrorPlayerNotFound)
		}

		json.Unmarshal([]byte(resp.Body()), &profile)
		return profile, nil
	}

	resp, err := c.Client.R().
		SetHeader("Accept", "application/json").
		Get(url)

	if err != nil {
		return []structs.Profile{}, err
	}

	if resp.StatusCode() == http.StatusNotFound {
		return []structs.Profile{}, errors.New(ErrorMatchNotFound)
	}

	json.Unmarshal([]byte(resp.Body()), &profile)
	return profile, nil
}
