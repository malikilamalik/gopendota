package gopendota

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetPlayerByAccountID(t *testing.T) {
	client := NewClient()
	t.Run("success_ok", func(t *testing.T) {
		// given
		accountID := "301750126"
		//when
		account, err := client.GetPlayerByAccountID(accountID)
		//then
		assert.NoError(t, err)
		assert.NotEqual(t, account.RankTier, 0)
	})

}

func TestGetPlayerWLByAccountID(t *testing.T) {
	client := NewClient()
	t.Run("success_ok", func(t *testing.T) {
		// given
		accountID := "301750126"
		//when
		account, err := client.GetPlayerWLByAccountID(accountID)
		//then
		assert.NoError(t, err)
		assert.NotEqual(t, account.Win, 0)
	})

}

func TestGetPlayerRecentMatchesByAccountID(t *testing.T) {
	client := NewClient()
	t.Run("success_ok", func(t *testing.T) {
		// given
		accountID := "301750126"
		//when
		account, err := client.GetPlayerRecentMatchesByAccountID(accountID)
		//then
		assert.NoError(t, err)
		assert.NotEqual(t, account[0].MatchID, 0)
	})

}
