package gopendota

import (
	"github.com/go-resty/resty/v2"
)

type Client struct {
	Client *resty.Client
	ApiKey *string
}

func NewClient() Service {
	client := Client{
		Client: resty.New(),
	}
	return client
}

func NewClientWithAPIKey(apiKey string) Service {
	client := Client{
		Client: resty.New(),
		ApiKey: &apiKey,
	}
	return client
}
