package main

import (
	"fmt"

	"github.com/malikilamalik/gopendota"
)

func main() {
	gopendota := gopendota.NewClient()
	matchID := "41231571"
	match, _ := gopendota.GetMatchByMatchID(matchID)

	fmt.Println(match.MatchID)
}
