package gopendota

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetProPlayer(t *testing.T) {
	client := NewClient()
	t.Run("success_ok", func(t *testing.T) {
		// given

		//when
		account, err := client.GetProPlayers()
		fmt.Println(account)
		//then
		assert.NoError(t, err)
		assert.NotEqual(t, account[0].AccountID, 0)
	})

}
