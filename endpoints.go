package gopendota

//Listing known Dota API Endpoints
var (
	//Base URL
	EndpointAPI           = "https://api.opendota.com/api/"
	EndpointMatches       = EndpointAPI + "matches/"
	EndpointPlayersByRank = EndpointAPI + "playersByRank/"
	EndpointPlayers       = EndpointAPI + "players/"
	EndpointProPlayers    = EndpointAPI + "proPlayers/"
	EndpointProMatches    = "https://api.opendota.com/api/"
	EndpointPublicMatches = EndpointAPI + "matches/"
	EndpointParsedMatches = EndpointAPI + "playersByRank/"
	EndpointExplorer      = EndpointAPI + "players/"
	EndpointMetadata      = EndpointAPI + "players/"
	EndpointDistributions = "https://api.opendota.com/api/"
	EndpointSearch        = EndpointAPI + "matches/"
	EndpointRankings      = EndpointAPI + "playersByRank/"
	EndpointBenchmarks    = EndpointAPI + "players/"
	EndpointStatus        = EndpointAPI + "players/"
	EndpointHealth        = "https://api.opendota.com/api/"
	EndpointRequest       = EndpointAPI + "matches/"
	EndpointFindMatches   = EndpointAPI + "playersByRank/"
	EndpointHeroes        = EndpointAPI + "players/"
	EndpointHeroStats     = EndpointAPI + "players/"
	EndpointLeagues       = "https://api.opendota.com/api/"
	EndpointTeams         = EndpointAPI + "matches/"
	EndpointReplays       = EndpointAPI + "playersByRank/"
	EndpointRecords       = EndpointAPI + "players/"
	EndpointLive          = EndpointAPI + "players/"
	EndpointScenarios     = EndpointAPI + "playersByRank/"
	EndpointSchema        = EndpointAPI + "players/"
	EndpointConstants     = EndpointAPI + "players/"

	// Matches
	EndpointMatch = func(mID string) (endpointMatch string) { return EndpointMatches + mID }

	// Players
	EndpointPlayer              = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID }
	EndpointPlayerWL            = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/wl" }
	EndpointPlayerRecentMatches = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/recentMatches" }
	EndpointPlayerMatches       = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/matches" }
	EndpointPlayerHeroes        = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/heroes" }
	EndpointPlayerPeers         = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/peers" }
	EndpointPlayerPros          = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/pros" }
	EndpointPlayerTotals        = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/totals" }
	EndpointPlayerCounts        = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/counts" }
	EndpointPlayerHistograms    = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/histograms" }
	EndpointPlayerWardMap       = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/wardmap" }
	EndpointPlayerWordCloud     = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/wordcloud" }
	EndpointPlayerRatings       = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/ratings" }
	EndpointPlayerRankings      = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/rankings" }
	EndpointPlayerRefresh       = func(aID string) (EndpointPlayer string) { return EndpointPlayers + aID + "/refresh" }
)
