package gopendota

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/malikilamalik/gopendota/structs"
)

//Error
var ErrorPlayerNotFound string = "players: players not found"

// GetPlayerByAccountID
func (c Client) GetPlayerByAccountID(accountID string) (player structs.Player, err error) {
	url := EndpointPlayer(accountID)

	if c.ApiKey != nil {
		resp, err := c.Client.R().
			SetQueryParams(map[string]string{
				"api_key": *c.ApiKey,
			}).
			SetHeader("Accept", "application/json").
			Get(url)

		if err != nil {
			return structs.Player{}, err
		}

		if resp.StatusCode() == http.StatusNotFound {
			return structs.Player{}, errors.New(ErrorPlayerNotFound)
		}

		json.Unmarshal([]byte(resp.Body()), &player)
		return player, nil
	}

	resp, err := c.Client.R().
		SetHeader("Accept", "application/json").
		Get(url)

	if err != nil {
		return structs.Player{}, err
	}

	if resp.StatusCode() == http.StatusNotFound {
		return structs.Player{}, errors.New(ErrorMatchNotFound)
	}

	json.Unmarshal([]byte(resp.Body()), &player)
	return player, nil
}

// GetPlayerWLByAccountID
func (c Client) GetPlayerWLByAccountID(accountID string) (playerWL structs.PlayerWL, err error) {
	url := EndpointPlayerWL(accountID)

	if c.ApiKey != nil {
		resp, err := c.Client.R().
			SetQueryParams(map[string]string{
				"api_key": *c.ApiKey,
			}).
			SetHeader("Accept", "application/json").
			Get(url)

		if err != nil {
			return structs.PlayerWL{}, err
		}

		if resp.StatusCode() == http.StatusNotFound {
			return structs.PlayerWL{}, errors.New(ErrorPlayerNotFound)
		}

		json.Unmarshal([]byte(resp.Body()), &playerWL)
		return playerWL, nil
	}

	resp, err := c.Client.R().
		SetHeader("Accept", "application/json").
		Get(url)

	if err != nil {
		return structs.PlayerWL{}, err
	}

	if resp.StatusCode() == http.StatusNotFound {
		return structs.PlayerWL{}, errors.New(ErrorMatchNotFound)
	}

	json.Unmarshal([]byte(resp.Body()), &playerWL)
	return playerWL, nil
}

// GetPlayerRecentMatchesByAccountID
func (c Client) GetPlayerRecentMatchesByAccountID(accountID string) (playerRecentMatches structs.PlayerRecentMatches, err error) {
	url := EndpointPlayerRecentMatches(accountID)

	if c.ApiKey != nil {
		resp, err := c.Client.R().
			SetQueryParams(map[string]string{
				"api_key": *c.ApiKey,
			}).
			SetHeader("Accept", "application/json").
			Get(url)

		if err != nil {
			return structs.PlayerRecentMatches{}, err
		}

		if resp.StatusCode() == http.StatusNotFound {
			return structs.PlayerRecentMatches{}, errors.New(ErrorPlayerNotFound)
		}

		json.Unmarshal([]byte(resp.Body()), &playerRecentMatches)
		return playerRecentMatches, nil
	}

	resp, err := c.Client.R().
		SetHeader("Accept", "application/json").
		Get(url)

	if err != nil {
		return structs.PlayerRecentMatches{}, err
	}

	if resp.StatusCode() == http.StatusNotFound {
		return structs.PlayerRecentMatches{}, errors.New(ErrorMatchNotFound)
	}

	json.Unmarshal([]byte(resp.Body()), &playerRecentMatches)
	return playerRecentMatches, nil
}
