package gopendota

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMatchByMatchID(t *testing.T) {
	client := NewClient()
	t.Run("success_ok", func(t *testing.T) {
		// given
		matchID := "6283429903"
		//when
		match, err := client.GetMatchByMatchID(matchID)

		//then
		assert.NoError(t, err)
		assert.NotEqual(t, match.MatchID, 0)
	})

}
