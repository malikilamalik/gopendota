package gopendota

import "github.com/malikilamalik/gopendota/structs"

type Service interface {
	//Matches
	GetMatchByMatchID(matchID string) (match structs.Match, err error)

	//Players
	GetPlayerByAccountID(accountID string) (player structs.Player, err error)
	GetPlayerWLByAccountID(accountID string) (playerWL structs.PlayerWL, err error)
	GetPlayerRecentMatchesByAccountID(accountID string) (playerRecentMatches structs.PlayerRecentMatches, err error)

	GetProPlayers() (profile []structs.Profile, err error)
}
